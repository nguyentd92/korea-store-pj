<?php

namespace App\Services;

class TransformImgPath{
    public static function do($path) {
        return $path ? config('appsettings.path').$path : config('appsettings.img_default');
    }
}