<?php

namespace App\Http\Controllers;

use App\Repositories\Constracts\BannerRepository;
use App\Repositories\Constracts\CategoryRepository;
use App\Repositories\Constracts\DealRepository;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function __construct(
        BannerRepository $bannerRepository,
        CategoryRepository $categoryRepository,
        DealRepository $dealRepository
    ) {
        $this->bannerRepository = $bannerRepository;
        $this->categoryRepository = $categoryRepository;
        $this->dealRepository = $dealRepository;
    }
    //
    public function __invoke()
    {
        $banners = $this->bannerRepository->all();
        $categories = $this->categoryRepository->with([
            'products' => function ($query) {
                $query->select(['id', 'name', 'unit_price', 'category_id', 'images']);
                return $query;
            }
        ])->all();

        $categories = $categories->map(function ($e) {
            $e->products = $e->products->map(function ($el) {
                $el->images = json_decode($el->images);
                $el->poster = isset($el->images[0]) ? config('appsettings.path') . $el->images[0] : config('appsettings.img_default');
                unset($el->images);
            });
            return $e;
        });

        $deals = $this->dealRepository->all()->take(2);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => [
                    'banners' => $banners,
                    'categories' => $categories,
                    'deals' => $deals
                ],
            ]);
        }
    }
}
