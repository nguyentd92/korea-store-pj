<?php

namespace App\Http\Controllers;

use App\Repositories\Constracts\OrderRepository;
use App\Repositories\Constracts\ProductRepository;
use ErrorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderingController extends Controller
{
    public function __construct(
        ProductRepository $productRepository,
        OrderRepository $orderRepository
    ) {
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
    }
    //
    public function __invoke(Request $request)
    {
        try {
            $user = $request->input('user');
            return $user;
            $order = $request->input('order');

            $idItems = [];
            $billTotal = 0;
            $attachArray = [];

            collect($order)->map(function ($item) use (&$idItems, &$billTotal, &$attachArray) {
                $productId = $item['id'];
                $quantity = $item['quantity'];
                $unitPrice = $item['unit_price'];
                $totalPrice = $unitPrice * $quantity;
                // Push Item ID to Order Items ID Array
                $idItems[] = $productId;

                // Increase Bill Total Price
                $billTotal += $totalPrice;

                // Prepare array to attach to Order - Items relationship
                $attachArray[$productId] = [
                    'quantity' => $quantity,
                    'unit_price' => $unitPrice,
                    'total' => $totalPrice
                ];
            });

            $orderReceipt = array_merge(
                $user,
                [
                    'bill_total' => $billTotal,
                    'bill_price' => $billTotal
                ]
            );
            DB::beginTransaction();
            $receipt = $this->orderRepository->create($orderReceipt);
            $receipt->products()->attach($attachArray);            
            DB::commit();
            if (request()->wantsJson()) {
                return response()->json([
                    'data' => [
                        'billTotal' => $billTotal,
                        'idItems' => $idItems,
                        'order' => $order,
                        'receipt' => $receipt,
                        'order_receipt' => $orderReceipt
                    ],
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ($e);
        }
    }
}
