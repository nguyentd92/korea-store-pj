<?php

namespace App\Http\Controllers;

use App\Repositories\Constracts\CategoryRepository;
use App\Repositories\Constracts\SettingRepository;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function __construct(
        CategoryRepository $categoryRepository,
        SettingRepository $settingRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->settingRepository = $settingRepository;
    }
    public function __invoke()
    {
        $categories = $this->categoryRepository->all();
        $settings = $this->settingRepository->findWhere([
            ['key', 'LIKE', 'shop%']
        ], ['key','value','display_name']);
        
        if (request()->wantsJson()) {
            return response()->json([
                'data' => [
                    'categories' => $categories,
                    'settings' => $settings
                ],
            ]);
        }
    }
}
