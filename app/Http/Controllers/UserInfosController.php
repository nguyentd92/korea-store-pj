<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserInfoCreateRequest;
use App\Http\Requests\UserInfoUpdateRequest;
use App\Repositories\Constracts\UserInfoRepository;
use App\Validators\UserInfoValidator;

/**
 * Class UserInfosController.
 *
 * @package namespace App\Http\Controllers;
 */
class UserInfosController extends Controller
{
    /**
     * @var UserInfoRepository
     */
    protected $repository;

    /**
     * @var UserInfoValidator
     */
    protected $validator;

    /**
     * UserInfosController constructor.
     *
     * @param UserInfoRepository $repository
     * @param UserInfoValidator $validator
     */
    public function __construct(UserInfoRepository $repository, UserInfoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $userInfos = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $userInfos,
            ]);
        }

        return view('userInfos.index', compact('userInfos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserInfoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UserInfoCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $userInfo = $this->repository->create($request->all());

            $response = [
                'message' => 'UserInfo created.',
                'data'    => $userInfo->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userInfo = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $userInfo,
            ]);
        }

        return view('userInfos.show', compact('userInfo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userInfo = $this->repository->find($id);

        return view('userInfos.edit', compact('userInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserInfoUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserInfoUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $userInfo = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'UserInfo updated.',
                'data'    => $userInfo->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'UserInfo deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'UserInfo deleted.');
    }
}
