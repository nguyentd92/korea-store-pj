<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface CategoryRepository extends RepositoryInterface
{
    //
}
