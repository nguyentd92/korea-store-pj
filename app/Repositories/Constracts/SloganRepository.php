<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SloganRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface SloganRepository extends RepositoryInterface
{
    //
}
