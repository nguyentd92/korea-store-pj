<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface OrderRepository extends RepositoryInterface
{
    //
}
