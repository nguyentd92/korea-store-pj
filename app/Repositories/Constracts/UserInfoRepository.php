<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserInfoRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface UserInfoRepository extends RepositoryInterface
{
    //
}
