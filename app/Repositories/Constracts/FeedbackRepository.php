<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FeedbackRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface FeedbackRepository extends RepositoryInterface
{
    //
}
