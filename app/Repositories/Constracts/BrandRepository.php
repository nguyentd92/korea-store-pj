<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BrandRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface BrandRepository extends RepositoryInterface
{
    //
}
