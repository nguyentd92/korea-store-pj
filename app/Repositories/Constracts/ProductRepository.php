<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface ProductRepository extends RepositoryInterface
{
    //
}
