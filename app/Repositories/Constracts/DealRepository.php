<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DealRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface DealRepository extends RepositoryInterface
{
    //
}
