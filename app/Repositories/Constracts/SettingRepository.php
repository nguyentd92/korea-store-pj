<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SettingRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface SettingRepository extends RepositoryInterface
{
    //
}
