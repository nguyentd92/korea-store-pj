<?php

namespace App\Repositories\Constracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BannerRepository.
 *
 * @package namespace App\Repositories\Constracts;
 */
interface BannerRepository extends RepositoryInterface
{
    //
}
