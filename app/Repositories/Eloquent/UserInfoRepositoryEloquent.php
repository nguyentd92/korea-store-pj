<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Constracts\UserInfoRepository;
use App\Entities\UserInfo;
use App\Validators\UserInfoValidator;

/**
 * Class UserInfoRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class UserInfoRepositoryEloquent extends BaseRepository implements UserInfoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserInfo::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return UserInfoValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
