<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Constracts\DealRepository;
use App\Entities\Deal;
use App\Repositories\BaseImgRepository;
use App\Validators\DealValidator;

/**
 * Class DealRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class DealRepositoryEloquent extends BaseImgRepository implements DealRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Deal::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return DealValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
