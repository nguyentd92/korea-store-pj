<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Constracts\SloganRepository;
use App\Entities\Slogan;
use App\Validators\SloganValidator;

/**
 * Class SloganRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class SloganRepositoryEloquent extends BaseRepository implements SloganRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Slogan::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return SloganValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
