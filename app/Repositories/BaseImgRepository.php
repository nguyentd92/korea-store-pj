<?php

namespace App\Repositories;

use App\Services\TransformImgPath;
use Prettus\Repository\Eloquent\BaseRepository;

abstract class BaseImgRepository extends BaseRepository
{
    public function find($id, $columns = ['*'])
    {
        $result = parent::find($id, $columns);

        if (isset($result['images'])) {
            $result->images = json_decode($result->images) ?? [];
            $result->images = collect($result->images)->map(function ($e) {
                return '\\storage\\' . $e;
            });
        }

        return $result;
    }

    public function all($columns = ['*'])
    {
        $result = parent::all($columns);

        $result->map(function ($e) {
            $e->image = TransformImgPath::do($e->image);
            $e->images = json_decode($e->images) ?? [];
            $e->images = collect($e->images)->map(function($el) {
                return TransformImgPath::do($el);
            });
            $e->poster = isset($e->images[0]) ? $e->images[0] : '';
        });
        
        return $result;
    }
}
