<?php

namespace App\Entities;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use Illuminate\Database\Eloquent\Model;

class ProductStatus extends Model implements Transformable
{
    //
    use TransformableTrait;

    protected $table = 'product_status';
    public $incrementing = false;
}
