<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserInfo.
 *
 * @package namespace App\Entities;
 */
class UserInfo extends Model implements Transformable
{
    use TransformableTrait;

    public $incrementing = false;
    protected $table = 'user_infos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
