<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Order.
 *
 * @package namespace App\Entities;
 */
class Order extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_email', 'full_name', 'phone', 'address', 'bill_total', 'bill_price'];

    public function products() {
        return $this->belongsToMany(Product::class, 'order_items', 'order_id', 'product_id', 'id', 'id');
    }
}
