<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\Constracts\OrderRepository::class, \App\Repositories\Eloquent\OrderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\CategoryRepository::class, \App\Repositories\Eloquent\CategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\BrandRepository::class, \App\Repositories\Eloquent\BrandRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\ProductRepository::class, \App\Repositories\Eloquent\ProductRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\UserInfoRepository::class, \App\Repositories\Eloquent\UserInfoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\FeedbackRepository::class, \App\Repositories\Eloquent\FeedbackRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\BannerRepository::class, \App\Repositories\Eloquent\BannerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\SettingRepository::class, \App\Repositories\Eloquent\SettingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\DealRepository::class, \App\Repositories\Eloquent\DealRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Constracts\SloganRepository::class, \App\Repositories\Eloquent\SloganRepositoryEloquent::class);
        //:end-bindings:
    }
}
