<?php

namespace App\Presenters;

use App\Transformers\SloganTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SloganPresenter.
 *
 * @package namespace App\Presenters;
 */
class SloganPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SloganTransformer();
    }
}
