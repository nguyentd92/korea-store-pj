<?php

namespace App\Presenters;

use App\Transformers\DealTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class DealPresenter.
 *
 * @package namespace App\Presenters;
 */
class DealPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DealTransformer();
    }
}
