<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Deal;

/**
 * Class DealTransformer.
 *
 * @package namespace App\Transformers;
 */
class DealTransformer extends TransformerAbstract
{
    /**
     * Transform the Deal entity.
     *
     * @param \App\Entities\Deal $model
     *
     * @return array
     */
    public function transform(Deal $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
