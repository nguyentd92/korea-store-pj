<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\UserInfo;

/**
 * Class UserInfoTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserInfoTransformer extends TransformerAbstract
{
    /**
     * Transform the UserInfo entity.
     *
     * @param \App\Entities\UserInfo $model
     *
     * @return array
     */
    public function transform(UserInfo $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
