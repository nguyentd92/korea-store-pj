<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Slogan;

/**
 * Class SloganTransformer.
 *
 * @package namespace App\Transformers;
 */
class SloganTransformer extends TransformerAbstract
{
    /**
     * Transform the Slogan entity.
     *
     * @param \App\Entities\Slogan $model
     *
     * @return array
     */
    public function transform(Slogan $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
