<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateProductsTable.
 */
class CreateProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
            $table->increments('id');
			$table->string('slug')->unique();
			$table->string('name');
			$table->unsignedInteger('category_id');
			$table->string('brand_id', 50);
			$table->decimal('unit_price');
			$table->float('discount_percent');
			$table->unsignedTinyInteger('status')->nullable();
			$table->longText('images')->nullable();
			$table->unsignedBigInteger('view_count')->default(0);
			$table->unsignedBigInteger('sole_count')->default(0);
			$table->unsignedBigInteger('like_count')->default(0);
			$table->unsignedBigInteger('like_id_list')->nullable();
			$table->longText('introduction_html')->nullable();
			$table->unsignedInteger('in_stocks')->default(0);
			$table->longText('log')->nullable();
			$table->softDeletes();

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}
}
