<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBrandsTable.
 */
class CreateBrandsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('brands', function(Blueprint $table) {
            $table->string('id',50)->primary();
			$table->string('name');
			$table->text('information')->nullable();
			$table->longText('introduction_html')->nullable();
			$table->text('images')->nullable();
			$table->string('logo_path')->nullable();
			$table->string('website')->nullable();
			$table->longText('log')->nullable();

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('brands');
	}
}
