<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUserInfosTable.
 */
class CreateUserInfosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_infos', function(Blueprint $table) {
			$table->string('email')->primary();
			$table->string('full_name');
			$table->string('phone', 20)->nullable();
			$table->string('address')->nullable();
			$table->unsignedBigInteger('order_count')->default(0);
			$table->boolean('gender')->default(0);
			$table->date('dob')->nullable();
			$table->softDeletes();
			
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_infos');
	}
}
