-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table koreastore.banners
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.banners: ~3 rows (approximately)
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
REPLACE INTO `banners` (`id`, `image`, `title`, `description`, `subtitle`, `created_at`, `updated_at`, `url`) VALUES
	(1, 'banners\\August2019\\gTqQNfFTS00KKpA0f35v.jpg', 'Chào mừng bạn đến với Korea Store', 'Với kinh nghiệm phân phối hàng nhập khẩu chính hãng, chúng tôi cam kết mang lại lợi ích tốt nhất cho khách hàng ở trong nước với các sản phẩm chính hãng từ các thương hiệu hàng đầu Hàn Quốc', '100% nhập khẩu từ Hàn Quốc', '2019-08-29 01:39:04', '2019-08-29 01:39:04', NULL),
	(2, 'banners\\August2019\\HieQXV9OQm5cbar05lG6.jpg', 'Chào mừng bạn đến với Korea Store', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Aldus PageMaker including versions of Lorem Ipsum', '2019-08-29 01:43:13', '2019-08-29 01:43:13', NULL),
	(3, 'banners\\August2019\\hJwU3WQHa2f1ymg5j6Qo.jpg', 'Chào mừng bạn đến với Korea Store', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Aldus PageMaker including versions of Lorem Ipsum', '2019-08-29 01:43:36', '2019-08-29 01:43:36', NULL);
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;

-- Dumping structure for table koreastore.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `introduction_html` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.brands: ~3 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
REPLACE INTO `brands` (`id`, `name`, `information`, `introduction_html`, `images`, `logo_path`, `website`, `log`, `created_at`, `updated_at`) VALUES
	('apple', 'Apple', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', NULL, '["brands\\\\August2019\\\\EvjozjLDvcQ0wTblxPQP.png","brands\\\\August2019\\\\b4c9Q7zCIz93mTfgAIzu.png"]', 'brands\\August2019\\4LRD4Fy0qVm1XO2B2ClY.png', 'https://www.apple.com/vn/', NULL, '2019-08-29 02:26:17', '2019-08-29 02:26:17'),
	('innisfree', 'Innisfree', NULL, NULL, NULL, 'brands\\September2019\\Hjb56uBuiwkNYbSpvr3Y.png', 'http://www.innisfree.com', NULL, '2019-09-03 03:05:10', '2019-09-03 03:05:10'),
	('samsung', 'Samsung', 'Samsung Company', NULL, '["brands\\\\August2019\\\\47QXb7gMi6ae1Q6rklBL.png"]', 'brands\\August2019\\TkcVtpHlFSMksWAIcomm.png', 'https://www.samsung.com/us/', NULL, '2019-08-08 13:24:36', '2019-08-08 13:24:36');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping structure for table koreastore.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.categories: ~5 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
REPLACE INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`, `images`) VALUES
	(3, NULL, 1, 'Other', 'other', '2019-08-08 12:48:29', '2019-08-08 12:48:29', NULL),
	(4, NULL, 1, 'Foods', 'foods', '2019-08-08 12:52:07', '2019-08-08 12:52:07', NULL),
	(5, 4, 3, 'Ginsen', 'ginsen', '2019-08-08 12:52:00', '2019-08-29 04:21:15', '["categories\\\\August2019\\\\x2k94nkDseojRzIL9dQb.jpg","categories\\\\August2019\\\\iNDqKyL19k2YYULr4GmQ.jpg","categories\\\\August2019\\\\zpba3Zrag8jn5PMe51pM.jpg","categories\\\\August2019\\\\Zmt7bbADztnAbgtClUby.jpg"]'),
	(6, NULL, 1, 'Phone', 'phone', '2019-08-29 02:27:00', '2019-08-29 04:20:55', '["categories\\\\August2019\\\\zdPnZA9Kqc1z8ekwQy6E.png","categories\\\\August2019\\\\sZnW0f3vyYuDP1gNmflg.jpg","categories\\\\August2019\\\\G1bA4FNhtj86MHpInHW7.jpg"]'),
	(7, NULL, 1, 'Mỹ phẩm', 'my-pham', '2019-09-03 02:59:30', '2019-09-03 02:59:30', NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table koreastore.data_rows
CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.data_rows: ~120 rows (approximately)
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
REPLACE INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
	(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
	(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
	(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
	(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
	(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
	(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
	(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsTo","column":"role_id","key":"id","label":"display_name","pivot_table":"roles","pivot":0}', 10),
	(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsToMany","column":"id","key":"id","label":"display_name","pivot_table":"user_roles","pivot":"1","taggable":"0"}', 11),
	(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
	(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
	(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
	(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
	(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
	(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
	(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
	(22, 4, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
	(23, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 0, 0, 0, 0, '{}', 2),
	(24, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 3),
	(25, 5, 'id', 'text', 'Id', 1, 1, 1, 0, 0, 0, '{}', 1),
	(26, 5, 'parent_id', 'text', 'Parent Id', 0, 1, 1, 1, 1, 1, '{}', 2),
	(27, 5, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{}', 3),
	(28, 5, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 4),
	(29, 5, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{}', 5),
	(30, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
	(31, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
	(32, 5, 'images', 'multiple_images', 'Images', 0, 1, 1, 1, 1, 1, '{}', 8),
	(33, 8, 'id', 'text', 'Id', 1, 1, 0, 0, 1, 0, '{}', 1),
	(34, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
	(35, 8, 'information', 'text_area', 'Information', 0, 0, 1, 1, 1, 1, '{}', 3),
	(36, 8, 'introduction_html', 'markdown_editor', 'Introduction Html', 0, 0, 1, 1, 1, 1, '{}', 4),
	(37, 8, 'images', 'multiple_images', 'Images', 0, 1, 1, 1, 1, 1, '{}', 5),
	(38, 8, 'logo_path', 'image', 'Logo Path', 0, 1, 1, 1, 1, 1, '{}', 6),
	(39, 8, 'website', 'text', 'Website', 0, 1, 1, 1, 1, 1, '{}', 7),
	(40, 8, 'log', 'text', 'Log', 0, 0, 1, 0, 0, 0, '{}', 8),
	(41, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 9),
	(42, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
	(43, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(44, 10, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{}', 2),
	(45, 10, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 3),
	(46, 10, 'category_id', 'select_dropdown', 'Category Id', 0, 1, 1, 1, 1, 1, '{}', 8),
	(47, 10, 'brand_id', 'select_dropdown', 'Brand Id', 0, 1, 1, 1, 1, 1, '{}', 9),
	(48, 10, 'unit_price', 'text', 'Unit Price', 0, 1, 1, 1, 1, 1, '{}', 10),
	(49, 10, 'discount_percent', 'text', 'Discount Percent', 0, 0, 1, 1, 1, 1, '{}', 11),
	(50, 10, 'status', 'select_dropdown', 'Status', 1, 0, 0, 0, 0, 0, '{}', 12),
	(51, 10, 'images', 'multiple_images', 'Images', 0, 0, 1, 1, 1, 1, '{}', 13),
	(52, 10, 'view_count', 'text', 'View Count', 1, 0, 1, 0, 0, 0, '{}', 14),
	(53, 10, 'sole_count', 'text', 'Sole Count', 1, 0, 1, 0, 0, 0, '{}', 15),
	(54, 10, 'like_count', 'text', 'Like Count', 1, 0, 1, 0, 0, 0, '{}', 16),
	(55, 10, 'liker_id_list', 'text', 'Liker Id List', 0, 0, 0, 0, 0, 0, '{}', 17),
	(56, 10, 'introduction_html', 'markdown_editor', 'Introduction Html', 0, 0, 1, 1, 1, 1, '{}', 18),
	(58, 10, 'in_stocks', 'text', 'In Stocks', 0, 1, 1, 1, 1, 1, '{}', 19),
	(59, 10, 'log', 'text', 'Log', 0, 0, 1, 0, 0, 0, '{}', 21),
	(60, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 23),
	(61, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 25),
	(62, 10, 'product_belongsto_brand_relationship', 'relationship', 'brands', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Entities\\\\Brand","table":"brands","type":"belongsTo","column":"brand_id","key":"id","label":"name","pivot_table":"brands","pivot":"0","taggable":"0"}', 6),
	(63, 10, 'product_belongsto_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Entities\\\\Category","table":"categories","type":"belongsTo","column":"category_id","key":"id","label":"name","pivot_table":"brands","pivot":"0","taggable":"0"}', 7),
	(64, 4, 'user_email', 'text', 'User Email', 1, 1, 0, 0, 0, 0, '{}', 2),
	(65, 4, 'status', 'text', 'Status', 1, 1, 0, 0, 0, 0, '{}', 3),
	(66, 4, 'information', 'text', 'Information', 0, 0, 0, 0, 0, 0, '{}', 4),
	(67, 4, 'bill_price', 'text', 'Bill Price', 1, 0, 0, 0, 0, 0, '{}', 5),
	(68, 4, 'bill_discount', 'text', 'Bill Discount', 1, 0, 0, 0, 0, 0, '{}', 6),
	(69, 4, 'bill_total', 'text', 'Bill Total', 1, 1, 0, 0, 0, 0, '{}', 7),
	(70, 4, 'log', 'text', 'Log', 0, 0, 0, 0, 0, 0, '{}', 8),
	(71, 4, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 11),
	(72, 4, 'order_belongstomany_product_relationship', 'relationship', 'products', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Entities\\\\Product","table":"products","type":"belongsToMany","column":"id","key":"id","label":"name","pivot_table":"order_items","pivot":"1","taggable":"on"}', 12),
	(76, 11, 'email', 'text', 'Email', 1, 1, 1, 0, 0, 0, '{}', 1),
	(77, 11, 'full_name', 'text', 'Full Name', 1, 1, 1, 1, 0, 0, '{}', 2),
	(78, 11, 'phone', 'text', 'Phone', 0, 1, 1, 1, 0, 0, '{}', 3),
	(79, 11, 'address', 'text', 'Address', 0, 1, 1, 0, 0, 0, '{}', 4),
	(80, 11, 'order_count', 'text', 'Order Count', 1, 1, 1, 0, 0, 0, '{}', 5),
	(81, 11, 'gender', 'radio_btn', 'Gender', 1, 1, 1, 1, 0, 0, '{}', 6),
	(82, 11, 'dob', 'date', 'Dob', 0, 1, 1, 1, 0, 0, '{}', 7),
	(83, 11, 'avt_path', 'image', 'Avatar Path', 0, 1, 0, 0, 0, 0, '{}', 8),
	(84, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 9),
	(85, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
	(86, 11, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 11),
	(87, 1, 'user_belongsto_user_info_relationship', 'relationship', 'user_infos', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Entities\\\\UserInfo","table":"user_infos","type":"belongsTo","column":"email","key":"email","label":"email","pivot_table":"brands","pivot":"0","taggable":null}', 13),
	(88, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(89, 12, 'title', 'text', 'Title', 1, 1, 1, 0, 0, 0, '{}', 2),
	(90, 12, 'content', 'text_area', 'Content', 1, 1, 1, 0, 0, 0, '{}', 3),
	(91, 12, 'type', 'text', 'Type', 1, 1, 1, 0, 0, 0, '{}', 4),
	(92, 12, 'status', 'text', 'Status', 1, 1, 1, 0, 0, 0, '{}', 5),
	(93, 12, 'replies', 'text', 'Replies', 0, 1, 1, 0, 0, 0, '{}', 6),
	(94, 12, 'user_email', 'text', 'User Email', 1, 1, 1, 0, 0, 0, '{}', 7),
	(95, 12, 'feedback_belongsto_user_info_relationship', 'relationship', 'user_infos', 0, 1, 1, 0, 0, 0, '{"model":"App\\\\Entities\\\\UserInfo","table":"user_infos","type":"belongsTo","column":"user_email","key":"email","label":"full_name","pivot_table":"brands","pivot":"0","taggable":"0"}', 8),
	(96, 10, 'product_belongstomany_order_item_relationship', 'relationship', 'order_items', 0, 0, 0, 0, 0, 0, '{"model":"App\\\\Entities\\\\Order","table":"order_items","type":"belongsToMany","column":"id","key":"order_id","label":"quantity","pivot_table":"order_items","pivot":"1","taggable":"on"}', 26),
	(97, 10, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 20),
	(98, 10, 'product_belongsto_product_status_relationship', 'relationship', 'product_status', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Entities\\\\ProductStatus","table":"product_status","type":"belongsTo","column":"status","key":"id","label":"name","pivot_table":"brands","pivot":"0","taggable":"0"}', 28),
	(99, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 8),
	(100, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
	(106, 16, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
	(108, 16, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
	(109, 16, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
	(110, 16, 'subtitle', 'text', 'Subtitle', 0, 1, 1, 1, 1, 1, '{}', 5),
	(111, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
	(112, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
	(113, 16, 'url', 'text', 'Url', 0, 1, 1, 1, 1, 1, '{}', 6),
	(114, 16, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 2),
	(115, 10, 'tutorial', 'markdown_editor', 'Tutorial', 0, 0, 1, 1, 1, 1, '{}', 22),
	(116, 10, 'advice', 'text', 'Advice', 0, 0, 1, 0, 0, 0, '{}', 24),
	(117, 10, 'rating', 'text', 'Rating', 0, 1, 1, 0, 0, 0, '{}', 27),
	(120, 10, 'info', 'markdown_editor', 'Info', 0, 0, 1, 1, 1, 1, '{}', 5),
	(121, 10, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, '{}', 4),
	(122, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(123, 17, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
	(124, 17, 'router', 'text', 'Router', 0, 1, 1, 1, 1, 1, '{}', 3),
	(125, 17, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 4),
	(126, 17, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 5),
	(127, 17, 'price', 'number', 'Price', 0, 1, 1, 1, 1, 1, '{}', 6),
	(128, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(129, 18, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
	(130, 18, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
	(131, 18, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 4),
	(132, 18, 'router', 'text', 'Router', 0, 1, 1, 1, 1, 1, '{}', 5);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

-- Dumping structure for table koreastore.data_types
CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.data_types: ~12 rows (approximately)
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
REPLACE INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
	(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2019-08-08 12:04:21', '2019-08-08 12:04:21'),
	(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-08-08 12:04:21', '2019-08-08 12:04:21'),
	(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-08-08 12:04:21', '2019-08-08 12:04:21'),
	(4, 'orders', 'orders', 'Order', 'Order', 'voyager-certificate', 'App\\Entities\\Order', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2019-08-08 12:07:26', '2019-08-08 14:26:54'),
	(5, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'App\\Entities\\Category', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2019-08-08 12:37:25', '2019-08-08 12:50:28'),
	(8, 'brands', 'brands', 'Brand', 'Brands', 'voyager-world', 'App\\Entities\\Brand', NULL, NULL, NULL, 1, 0, '{"order_column":"id","order_display_column":"id","order_direction":"asc","default_search_key":null,"scope":null}', '2019-08-08 13:19:23', '2019-08-08 13:26:31'),
	(10, 'products', 'products', 'Product', 'Products', 'voyager-phone', 'App\\Entities\\Product', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2019-08-08 13:31:45', '2019-09-03 05:00:19'),
	(11, 'user_infos', 'user-infos', 'User Info', 'User Infos', NULL, 'App\\Entities\\UserInfo', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2019-08-08 15:05:06', '2019-08-08 15:05:06'),
	(12, 'feedbacks', 'feedbacks', 'Feedback', 'Feedbacks', NULL, 'App\\Entities\\Feedback', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2019-08-08 15:22:07', '2019-08-08 16:34:01'),
	(16, 'banners', 'banners', 'Banner', 'Banners', 'voyager-photo', 'App\\Entities\\Banner', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}', '2019-08-29 01:34:32', '2019-08-29 02:07:57'),
	(17, 'deals', 'deals', 'Deal', 'Deals', NULL, 'App\\Entities\\Deal', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2019-09-04 01:36:48', '2019-09-04 01:36:48'),
	(18, 'slogans', 'slogans', 'Slogan', 'Slogans', NULL, 'App\\Entities\\Slogan', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}', '2019-09-04 03:40:26', '2019-09-04 03:40:26');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

-- Dumping structure for table koreastore.deals
CREATE TABLE IF NOT EXISTS `deals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `router` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.deals: ~2 rows (approximately)
/*!40000 ALTER TABLE `deals` DISABLE KEYS */;
REPLACE INTO `deals` (`id`, `image`, `router`, `title`, `description`, `price`, `created_at`, `updated_at`) VALUES
	(1, 'deals\\September2019\\1uzLqpxm6f97joUZmEsk.jpg', NULL, 'Samsung S10+', 'Giá chỉ từ 17.990.000 đ', 17990000, '2019-09-04 01:39:42', '2019-09-04 01:39:42'),
	(2, 'deals\\September2019\\QeM7J8hF2CTc92TYfBEz.jpg', NULL, 'Samsung S10', 'Giá chỉ từ 15.990.000 đ', 15990000, '2019-09-04 01:41:31', '2019-09-04 01:41:31');
/*!40000 ALTER TABLE `deals` ENABLE KEYS */;

-- Dumping structure for table koreastore.feedbacks
CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('feedback','complained') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'feedback',
  `status` binary(1) NOT NULL DEFAULT '0',
  `replies` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.feedbacks: ~0 rows (approximately)
/*!40000 ALTER TABLE `feedbacks` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedbacks` ENABLE KEYS */;

-- Dumping structure for table koreastore.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.menus: ~0 rows (approximately)
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
REPLACE INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', '2019-08-08 12:04:22', '2019-08-08 13:38:43');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping structure for table koreastore.menu_items
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.menu_items: ~22 rows (approximately)
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
REPLACE INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
	(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-08-08 12:04:22', '2019-08-08 12:04:22', 'voyager.dashboard', NULL),
	(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2019-08-08 12:04:22', '2019-08-08 15:24:56', 'voyager.media.index', NULL),
	(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 18, 3, '2019-08-08 12:04:22', '2019-08-29 01:30:01', 'voyager.users.index', NULL),
	(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 19, 1, '2019-08-08 12:04:22', '2019-08-08 14:23:12', 'voyager.roles.index', NULL),
	(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 6, '2019-08-08 12:04:22', '2019-08-08 15:24:47', NULL, NULL),
	(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2019-08-08 12:04:22', '2019-08-29 01:30:02', 'voyager.menus.index', NULL),
	(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-08-08 12:04:22', '2019-08-29 01:30:02', 'voyager.database.index', NULL),
	(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-08-08 12:04:22', '2019-08-29 01:30:02', 'voyager.compass.index', NULL),
	(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-08-08 12:04:22', '2019-08-29 01:30:02', 'voyager.bread.index', NULL),
	(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 7, '2019-08-08 12:04:22', '2019-08-08 15:24:47', 'voyager.settings.index', NULL),
	(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-08-08 12:04:22', '2019-08-29 01:30:02', 'voyager.hooks', NULL),
	(12, 1, 'Order', '', '_self', 'voyager-certificate', NULL, NULL, 2, '2019-08-08 12:07:26', '2019-08-08 14:22:02', 'voyager.orders.index', NULL),
	(13, 1, 'Categories', '', '_self', 'voyager-categories', NULL, 18, 5, '2019-08-08 12:37:25', '2019-08-29 01:30:02', 'voyager.categories.index', NULL),
	(16, 1, 'Brands', '', '_self', 'voyager-photo', '#000000', 18, 6, '2019-08-08 13:19:23', '2019-08-29 01:32:13', 'voyager.brands.index', 'null'),
	(17, 1, 'Products', '', '_self', 'voyager-phone', '#000000', 18, 2, '2019-08-08 13:31:45', '2019-08-29 01:30:01', 'voyager.products.index', 'null'),
	(18, 1, 'Entities', '', '_self', 'voyager-list', '#000000', NULL, 5, '2019-08-08 13:37:49', '2019-08-08 15:24:56', NULL, ''),
	(19, 1, 'Systems', '', '_self', 'voyager-lock', '#000000', NULL, 8, '2019-08-08 14:23:02', '2019-08-08 15:24:47', NULL, ''),
	(20, 1, 'User Infos', '', '_self', 'voyager-book', '#000000', 18, 4, '2019-08-08 15:05:06', '2019-08-29 01:30:02', 'voyager.user-infos.index', 'null'),
	(21, 1, 'Feedbacks', '', '_self', 'voyager-mail', '#000000', NULL, 3, '2019-08-08 15:22:07', '2019-08-08 15:24:47', 'voyager.feedbacks.index', 'null'),
	(23, 1, 'Banners', '', '_self', 'voyager-photo', NULL, NULL, 9, '2019-08-29 01:34:33', '2019-08-29 01:34:33', 'voyager.banners.index', NULL),
	(24, 1, 'Deals', '', '_self', NULL, NULL, NULL, 10, '2019-09-04 01:36:49', '2019-09-04 01:36:49', 'voyager.deals.index', NULL),
	(25, 1, 'Slogans', '', '_self', NULL, NULL, NULL, 11, '2019-09-04 03:40:26', '2019-09-04 03:40:26', 'voyager.slogans.index', NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

-- Dumping structure for table koreastore.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.migrations: ~27 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_01_01_000000_add_voyager_user_fields', 1),
	(4, '2016_01_01_000000_create_data_types_table', 1),
	(5, '2016_01_01_000000_create_pages_table', 1),
	(6, '2016_01_01_000000_create_posts_table', 1),
	(7, '2016_02_15_204651_create_categories_table', 1),
	(8, '2016_05_19_173453_create_menu_table', 1),
	(9, '2016_10_21_190000_create_roles_table', 1),
	(10, '2016_10_21_190000_create_settings_table', 1),
	(11, '2016_11_30_135954_create_permission_table', 1),
	(12, '2016_11_30_141208_create_permission_role_table', 1),
	(13, '2016_12_26_201236_data_types__add__server_side', 1),
	(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
	(15, '2017_01_14_005015_create_translations_table', 1),
	(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
	(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
	(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
	(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
	(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
	(21, '2017_08_05_000000_add_group_to_settings_table', 1),
	(22, '2017_11_26_013050_add_user_role_relationship', 1),
	(23, '2017_11_26_015000_create_user_roles_table', 1),
	(24, '2018_03_11_000000_add_user_settings', 1),
	(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
	(26, '2018_03_16_000000_make_settings_value_nullable', 1),
	(27, '2019_08_08_104340_create_orders_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table koreastore.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'CREATED',
  `information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_price` decimal(10,0) NOT NULL,
  `bill_discount` float NOT NULL DEFAULT 0,
  `bill_total` decimal(10,0) NOT NULL,
  `log` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `full_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table koreastore.order_items
CREATE TABLE IF NOT EXISTS `order_items` (
  `order_id` bigint(20) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `discount_percent` float NOT NULL DEFAULT 0,
  `unit_price` decimal(10,0) NOT NULL,
  `total` decimal(10,0) NOT NULL,
  `log` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `order_items_order_id_index` (`order_id`),
  KEY `order_items_product_id_index` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.order_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;

-- Dumping structure for table koreastore.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.pages: ~0 rows (approximately)
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for table koreastore.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table koreastore.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.permissions: ~76 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
REPLACE INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
	(1, 'browse_admin', NULL, '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(2, 'browse_bread', NULL, '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(3, 'browse_database', NULL, '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(4, 'browse_media', NULL, '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(5, 'browse_compass', NULL, '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(6, 'browse_menus', 'menus', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(7, 'read_menus', 'menus', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(8, 'edit_menus', 'menus', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(9, 'add_menus', 'menus', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(10, 'delete_menus', 'menus', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(11, 'browse_roles', 'roles', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(12, 'read_roles', 'roles', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(13, 'edit_roles', 'roles', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(14, 'add_roles', 'roles', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(15, 'delete_roles', 'roles', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(16, 'browse_users', 'users', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(17, 'read_users', 'users', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(18, 'edit_users', 'users', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(19, 'add_users', 'users', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(20, 'delete_users', 'users', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(21, 'browse_settings', 'settings', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(22, 'read_settings', 'settings', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(23, 'edit_settings', 'settings', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(24, 'add_settings', 'settings', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(25, 'delete_settings', 'settings', '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(26, 'browse_hooks', NULL, '2019-08-08 12:04:22', '2019-08-08 12:04:22'),
	(27, 'browse_orders', 'orders', '2019-08-08 12:07:26', '2019-08-08 12:07:26'),
	(28, 'read_orders', 'orders', '2019-08-08 12:07:26', '2019-08-08 12:07:26'),
	(29, 'edit_orders', 'orders', '2019-08-08 12:07:26', '2019-08-08 12:07:26'),
	(30, 'add_orders', 'orders', '2019-08-08 12:07:26', '2019-08-08 12:07:26'),
	(31, 'delete_orders', 'orders', '2019-08-08 12:07:26', '2019-08-08 12:07:26'),
	(32, 'browse_categories', 'categories', '2019-08-08 12:37:25', '2019-08-08 12:37:25'),
	(33, 'read_categories', 'categories', '2019-08-08 12:37:25', '2019-08-08 12:37:25'),
	(34, 'edit_categories', 'categories', '2019-08-08 12:37:25', '2019-08-08 12:37:25'),
	(35, 'add_categories', 'categories', '2019-08-08 12:37:25', '2019-08-08 12:37:25'),
	(36, 'delete_categories', 'categories', '2019-08-08 12:37:25', '2019-08-08 12:37:25'),
	(37, 'browse_brand', 'brand', '2019-08-08 13:08:50', '2019-08-08 13:08:50'),
	(38, 'read_brand', 'brand', '2019-08-08 13:08:50', '2019-08-08 13:08:50'),
	(39, 'edit_brand', 'brand', '2019-08-08 13:08:50', '2019-08-08 13:08:50'),
	(40, 'add_brand', 'brand', '2019-08-08 13:08:50', '2019-08-08 13:08:50'),
	(41, 'delete_brand', 'brand', '2019-08-08 13:08:50', '2019-08-08 13:08:50'),
	(42, 'browse_brands', 'brands', '2019-08-08 13:19:23', '2019-08-08 13:19:23'),
	(43, 'read_brands', 'brands', '2019-08-08 13:19:23', '2019-08-08 13:19:23'),
	(44, 'edit_brands', 'brands', '2019-08-08 13:19:23', '2019-08-08 13:19:23'),
	(45, 'add_brands', 'brands', '2019-08-08 13:19:23', '2019-08-08 13:19:23'),
	(46, 'delete_brands', 'brands', '2019-08-08 13:19:23', '2019-08-08 13:19:23'),
	(47, 'browse_products', 'products', '2019-08-08 13:31:45', '2019-08-08 13:31:45'),
	(48, 'read_products', 'products', '2019-08-08 13:31:45', '2019-08-08 13:31:45'),
	(49, 'edit_products', 'products', '2019-08-08 13:31:45', '2019-08-08 13:31:45'),
	(50, 'add_products', 'products', '2019-08-08 13:31:45', '2019-08-08 13:31:45'),
	(51, 'delete_products', 'products', '2019-08-08 13:31:45', '2019-08-08 13:31:45'),
	(52, 'browse_user_infos', 'user_infos', '2019-08-08 15:05:06', '2019-08-08 15:05:06'),
	(53, 'read_user_infos', 'user_infos', '2019-08-08 15:05:06', '2019-08-08 15:05:06'),
	(54, 'edit_user_infos', 'user_infos', '2019-08-08 15:05:06', '2019-08-08 15:05:06'),
	(55, 'add_user_infos', 'user_infos', '2019-08-08 15:05:06', '2019-08-08 15:05:06'),
	(56, 'delete_user_infos', 'user_infos', '2019-08-08 15:05:06', '2019-08-08 15:05:06'),
	(57, 'browse_feedbacks', 'feedbacks', '2019-08-08 15:22:07', '2019-08-08 15:22:07'),
	(58, 'read_feedbacks', 'feedbacks', '2019-08-08 15:22:07', '2019-08-08 15:22:07'),
	(59, 'edit_feedbacks', 'feedbacks', '2019-08-08 15:22:07', '2019-08-08 15:22:07'),
	(60, 'add_feedbacks', 'feedbacks', '2019-08-08 15:22:07', '2019-08-08 15:22:07'),
	(61, 'delete_feedbacks', 'feedbacks', '2019-08-08 15:22:07', '2019-08-08 15:22:07'),
	(67, 'browse_banners', 'banners', '2019-08-29 01:34:32', '2019-08-29 01:34:32'),
	(68, 'read_banners', 'banners', '2019-08-29 01:34:32', '2019-08-29 01:34:32'),
	(69, 'edit_banners', 'banners', '2019-08-29 01:34:32', '2019-08-29 01:34:32'),
	(70, 'add_banners', 'banners', '2019-08-29 01:34:32', '2019-08-29 01:34:32'),
	(71, 'delete_banners', 'banners', '2019-08-29 01:34:32', '2019-08-29 01:34:32'),
	(72, 'browse_deals', 'deals', '2019-09-04 01:36:49', '2019-09-04 01:36:49'),
	(73, 'read_deals', 'deals', '2019-09-04 01:36:49', '2019-09-04 01:36:49'),
	(74, 'edit_deals', 'deals', '2019-09-04 01:36:49', '2019-09-04 01:36:49'),
	(75, 'add_deals', 'deals', '2019-09-04 01:36:49', '2019-09-04 01:36:49'),
	(76, 'delete_deals', 'deals', '2019-09-04 01:36:49', '2019-09-04 01:36:49'),
	(77, 'browse_slogans', 'slogans', '2019-09-04 03:40:26', '2019-09-04 03:40:26'),
	(78, 'read_slogans', 'slogans', '2019-09-04 03:40:26', '2019-09-04 03:40:26'),
	(79, 'edit_slogans', 'slogans', '2019-09-04 03:40:26', '2019-09-04 03:40:26'),
	(80, 'add_slogans', 'slogans', '2019-09-04 03:40:26', '2019-09-04 03:40:26'),
	(81, 'delete_slogans', 'slogans', '2019-09-04 03:40:26', '2019-09-04 03:40:26');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table koreastore.permission_role
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.permission_role: ~75 rows (approximately)
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
REPLACE INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 1),
	(23, 1),
	(24, 1),
	(25, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(30, 1),
	(31, 1),
	(32, 1),
	(33, 1),
	(34, 1),
	(35, 1),
	(36, 1),
	(37, 1),
	(38, 1),
	(39, 1),
	(40, 1),
	(41, 1),
	(42, 1),
	(43, 1),
	(44, 1),
	(45, 1),
	(46, 1),
	(47, 1),
	(48, 1),
	(49, 1),
	(50, 1),
	(51, 1),
	(52, 1),
	(53, 1),
	(54, 1),
	(55, 1),
	(56, 1),
	(57, 1),
	(58, 1),
	(59, 1),
	(60, 1),
	(61, 1),
	(67, 1),
	(68, 1),
	(69, 1),
	(70, 1),
	(71, 1),
	(72, 1),
	(73, 1),
	(74, 1),
	(75, 1),
	(76, 1),
	(77, 1),
	(78, 1),
	(79, 1),
	(80, 1),
	(81, 1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping structure for table koreastore.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.posts: ~0 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table koreastore.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `brand_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_price` decimal(10,0) DEFAULT NULL,
  `discount_percent` float DEFAULT 0,
  `status` varchar(17) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'launched',
  `images` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_count` bigint(20) unsigned NOT NULL DEFAULT 0,
  `sole_count` bigint(20) unsigned NOT NULL DEFAULT 0,
  `like_count` bigint(20) unsigned NOT NULL DEFAULT 0,
  `liker_id_list` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `introduction_html` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_stocks` bigint(20) unsigned DEFAULT 0,
  `log` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `tutorial` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advice` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `info` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.products: ~11 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
REPLACE INTO `products` (`id`, `slug`, `name`, `category_id`, `brand_id`, `unit_price`, `discount_percent`, `status`, `images`, `view_count`, `sole_count`, `like_count`, `liker_id_list`, `introduction_html`, `in_stocks`, `log`, `created_at`, `updated_at`, `deleted_at`, `tutorial`, `advice`, `rating`, `info`, `description`) VALUES
	(2, 'samsung-s10-plus', 'Samsung S10 plus', 3, 'samsung', 10000000, NULL, 'launched', NULL, 0, 0, 0, NULL, NULL, 10, NULL, '2019-08-08 16:17:32', '2019-08-08 16:17:32', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'ginsen', 'Ginsen Korea', 5, 'samsung', 10000000, NULL, 'launched', '["products\\\\August2019\\\\jGeh1aHGdCAAq31Ys5NF.png","products\\\\August2019\\\\pJEjlNEn06CPHenk7UU9.png","products\\\\August2019\\\\23N8YfvIcJiupWmPgG55.png","products\\\\August2019\\\\EdGRqfnQnLgVR70wkiNX.png","products\\\\August2019\\\\u5vvWHhVnX4pVcA2MImX.png","products\\\\August2019\\\\HWIwYxEIqsAeIrZJaMyN.png","products\\\\August2019\\\\fYVgLScfhmbaT9DRuUYu.png","products\\\\August2019\\\\sq3Kj6ugQVktvlTdQW4G.png","products\\\\August2019\\\\f63ILqVfloAHxOhDb1FU.png","products\\\\August2019\\\\xngmLeCEHqyYCF2g8r8J.png","products\\\\August2019\\\\U933nyuJjTnL6qJJqbtc.png","products\\\\August2019\\\\YV6JWbrYR3Jl73KqZIoT.png","products\\\\August2019\\\\sCDOc1LOXs02wamtFKv0.png","products\\\\August2019\\\\bZT1YtwB5yL2Aqz33puq.png","products\\\\August2019\\\\CLAaLLkiZ2WbanvgwW3o.png","products\\\\August2019\\\\QuxH6q0u3tNcAWn7MaE5.png","products\\\\August2019\\\\PPi6P6o17ZvaE5a70JXl.png","products\\\\August2019\\\\NbRAnzbUqFJGsE0xsRfK.png","products\\\\August2019\\\\3xkowhG4id5JM7rLqS13.png","products\\\\August2019\\\\ofWekGuEnJMSOO3bmuH0.png"]', 0, 0, 0, NULL, NULL, 10, NULL, '2019-08-08 16:20:18', '2019-08-28 10:19:35', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'samsung-s10', 'Samsung S10', 3, 'samsung', 10000000, NULL, 'launched', '["products\\\\August2019\\\\amRD89OsfRxWCBtgZxMS.png","products\\\\August2019\\\\KjLO5jw1bDngOsE1jIpr.png","products\\\\August2019\\\\bT0DCCRXLTV4aSToehiO.png"]', 0, 0, 0, NULL, NULL, NULL, NULL, '2019-08-08 16:23:10', '2019-08-28 10:01:45', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'iphone-xs-max', 'Iphone XS MAX', 6, 'apple', 23000000, NULL, 'launched', '["products\\\\August2019\\\\wuDikcNxAtKRKDTamxhv.png","products\\\\August2019\\\\FiV2xJZUJYZbusT8WGQc.png"]', 0, 0, 0, NULL, NULL, 20, NULL, '2019-08-29 02:28:17', '2019-08-29 02:28:17', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'iphone-xr', 'Iphone XR', 6, 'apple', 17000000, NULL, 'launched', '["products\\\\August2019\\\\WAoZYaHiiiioaypc9lb0.png","products\\\\August2019\\\\EWUTUQos1VaVKsRUCpDw.png"]', 0, 0, 0, NULL, NULL, 20, NULL, '2019-08-29 02:29:02', '2019-08-29 02:29:02', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'iphone-xs', 'Iphone XS', 6, 'apple', 19500000, NULL, 'launched', '["products\\\\August2019\\\\andBlOBndgZXG4zLeRO9.png","products\\\\August2019\\\\B2p8ia09x8o4pxNQyiCX.png"]', 0, 0, 0, NULL, NULL, 10, NULL, '2019-08-29 02:29:40', '2019-08-29 02:29:40', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'samsung-s10plus', 'Samsung S10 plus', 6, 'samsung', 16000000, NULL, 'launched', '["products\\\\August2019\\\\FJLVYnmMr0fdgZsvWBEg.png","products\\\\August2019\\\\zaQKJgnptEjm4pSf3S08.jpg"]', 0, 0, 0, NULL, NULL, 20, NULL, '2019-08-29 02:30:43', '2019-08-29 02:31:07', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'samsungs10', 'Samsung S10', 6, 'samsung', 21000000, NULL, 'launched', '["products\\\\August2019\\\\4N3Y6tOGEPJUVW1Hb9S1.png","products\\\\August2019\\\\Jm6IVxqxkz904rDSOfdV.png"]', 0, 0, 0, NULL, NULL, NULL, NULL, '2019-08-29 02:32:06', '2019-08-29 02:32:06', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'iphone-x', 'Iphone X', 6, 'apple', 12000000, NULL, 'launched', '["products\\\\August2019\\\\KsdloMW4NGF26rdjcWt8.png","products\\\\August2019\\\\BHBYbnE755KwGZBTLhfG.png"]', 0, 0, 0, NULL, NULL, NULL, NULL, '2019-08-29 02:33:08', '2019-08-29 02:33:08', NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'Innisfree-Perfect-UV-Protection-Cream-Long-Lasting/For-Oily-Skin', 'Innisfree Perfect UV Protection Cream Long Lasting/For Oily Skin', 7, 'innisfree', 0, NULL, 'launched', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '2019-09-03 03:07:19', '2019-09-03 04:58:38', NULL, '**Where does it come from?**\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', NULL, NULL, '**Thành phần chính**\r\n* Dầu hoa hướng dương và chiết xuất trà xanh: Bảo vệ làn da, chống lại các tia cực tím ngoài trời, tăng cường và giúp da khỏe mạnh hơn.\r\n* Hạt Blue weed và Baloon Vine: Có công dụng làm dịu da, mang lại cảm giác tươi tắn dù hoạt động ngoài trời cả ngày.\r\n* Nho và hướng dương xám: Giúp chăm sóc làn da mệt mỏi và làm mát da khi dùng.\r\n* Và một số thành phần khác: Water, Cyclopentasiloxane, Zinc Oxide, Ethylhexyl Methoxycinnamate, Homosalate, Glycerin,..\r\n* Không chứa các thành phần từ nguồn gốc động vật, dầu khoáng, hương thơm nhân tạo, chất bảo quản hay elthanol, lành tính cho da\r\n\r\n**Ưu điểm**\r\n* Innisfree Perfect UV Protection Cream Long Lasting/For Oily Skin 50ml là loại kem chống nắng có chỉ số SPF 50+, PA+++ hoàn hảo phù hợp da hỗn hợp đến da dầu hoặc da nhạy cảm. \r\n* Phù hợp da hỗn hợp thiên dầu đến da dầu.\r\n* Chỉ số chống nắng cao, SPF 50+, PA+++, bảo vệ da khỏi các tác động bởi môi trường bên ngoài.\r\n* Kem có ưu điểm là sau khi sử dụng sẽ cho bạn một làn da trắng hồng, che khuyết điểm tương đối.\r\n* Không gây nặng mặt, không làm bí tắc lỗ chân lông\r\n* Có thể thay thế lớp kem lót trang điểm hằng ngày\r\n* Có khả năng kiềm dầu tốt\r\n\r\n**Nhược điểm**\r\n* Dễ gây mụn ẩn nếu tẩy trang không kỹ.\r\n* Dễ mua nhầm hàng giả.', 'Innisfree Perfect UV Protection Cream Long Lasting/For Oily Skin 50ml là loại kem chống nắng có chỉ số SPF 50+, PA+++ hoàn hảo phù hợp da hỗn hợp đến da dầu hoặc da nhạy cảm.'),
	(14, 'Innisfree-Daily-UV-Protection-Cream-No-sebum', 'Innisfree Daily UV Protection Cream No sebum', 7, 'innisfree', NULL, NULL, 'launched', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '2019-09-03 05:02:17', '2019-09-03 05:02:17', NULL, NULL, NULL, NULL, '**Thành phần chính**\r\n* Dầu hoa hướng dương và chiết xuất trà xanh: Có công dụng giúp bảo vệ làn da, chống lại các tia cực tím,  giúp da khỏe mạnh hơn.\r\n* Rau sam và rau mà giúp làm dịu da khi da.\r\n* 100% bộ lọc khoán nhẹ nhàng chăm sóc làn da\r\n* Bột xốp giúp kiểm soát dầu và mồ hôi trên da.\r\n* Và một số thành phần khác: Cyclopentasiloxane, Water, Zinc Oxide (Ci 77947), Titanium Dioxide (Ci 77891), Methyl Methacrylate Crosspolymer …\r\n	\r\n**Ưu điểm**\r\n* Phù hợp cho những bạn có làn da dầu hoặc hỗn hợp thiên dầu vì chất kem không gây nhờn và bí da, không chứa những thành phần dễ gây nổi mụn.\r\n* Không gây nặng mặt, thích hợp để đi làm, đi học hằng ngày\r\n* Thấm vào da, không gây cảm giác nhờn rít và khả năng kiềm dầu tốt\r\n* Sản phẩm giúp nâng tone vừa cho da và có độ che phủ nhẹ.\r\n* Có khả năng kiềm dầu ở mức khá.	\r\n\r\n**Nhược điểm**\r\n* Không làm bật tone da.\r\n* Do chỉ số chống nắng vừa phải, nên phải thoa lại sau vài giờ đồng hồ.', 'Innisfree Daily UV Protection Cream No sebum SPF35 PA+++ là sản phẩm mới với nhiều cải tiến hơn so với phiên bản cũ Innisfree Eco Safety No Sebum Sunblock SPF35. Kem chống nắng có thiết kế dạng kem lỏng. Chỉ số chống nắng vừa phải thích hợp đi lại trong thành phố, đi làm hoặc đi học.');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table koreastore.product_status
CREATE TABLE IF NOT EXISTS `product_status` (
  `id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.product_status: ~3 rows (approximately)
/*!40000 ALTER TABLE `product_status` DISABLE KEYS */;
REPLACE INTO `product_status` (`id`, `name`) VALUES
	('launched', 'Launced'),
	('outdated', 'Outdated'),
	('reverse', 'Reverse');
/*!40000 ALTER TABLE `product_status` ENABLE KEYS */;

-- Dumping structure for table koreastore.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
REPLACE INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Administrator', '2019-08-08 11:57:26', '2019-08-08 11:57:26'),
	(2, 'user', 'Normal User', '2019-08-08 12:04:22', '2019-08-08 12:04:22');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table koreastore.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.settings: ~19 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
REPLACE INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
	(1, 'site.title', 'Site Title', 'Korea Store', '', 'text', 1, 'Site'),
	(2, 'site.description', 'Site Description', 'Chuyên nhập khẩu phân phối các sản phẩm đến từ Hàn Quóc', '', 'text', 2, 'Site'),
	(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
	(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
	(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
	(6, 'admin.title', 'Admin Title', 'Korea Store', '', 'text', 1, 'Admin'),
	(7, 'admin.description', 'Admin Description', 'Welcome to Korea Store', '', 'text', 2, 'Admin'),
	(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
	(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
	(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
	(13, 'shop.phone', 'phone', '0935418749', NULL, 'text', 6, 'Shop'),
	(14, 'shop.address', 'Address', '20 Lê Lợi - Huế', NULL, 'text', 7, 'Shop'),
	(15, 'shop.email', 'Email', 'nguyentd92@gmail.com', NULL, 'text', 8, 'Shop'),
	(16, 'shop.facebook', 'Facebook', '#', NULL, 'text', 9, 'Shop'),
	(17, 'shop.twitter', 'Twitter', '#', NULL, 'text', 10, 'Shop'),
	(18, 'shop.fanpage', 'Fanpage', '#', NULL, 'text', 11, 'Shop'),
	(19, 'shop.brand', 'Brand', 'Korea Store', NULL, 'text', 12, 'Shop'),
	(20, 'shop.logo', 'Logo', '', NULL, 'image', 13, 'Shop'),
	(21, 'shop.info', 'Information', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, 'text_area', 14, 'Shop');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table koreastore.slogans
CREATE TABLE IF NOT EXISTS `slogans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `router` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.slogans: ~0 rows (approximately)
/*!40000 ALTER TABLE `slogans` DISABLE KEYS */;
/*!40000 ALTER TABLE `slogans` ENABLE KEYS */;

-- Dumping structure for table koreastore.translations
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.translations: ~0 rows (approximately)
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

-- Dumping structure for table koreastore.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `role_id`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `name`) VALUES
	(1, 1, 'your@email.com', 'users/default.png', NULL, '$2y$10$ed46hhw4yknomsAP75q8DOa5yvTZ2am5ECekGFWinzHkBvSK1tQcq', NULL, '{"locale":"en"}', '2019-08-08 11:57:26', '2019-08-08 16:28:01', 'Admin'),
	(2, 2, 'user@email.com', 'users/default.png', NULL, '$2y$10$EjNw473Dl42W/P6luM0VFOXyh8n7lB0XfYBjrI.ZJJMkH7nnP.CjK', NULL, '{"locale":"en"}', '2019-08-08 14:50:14', '2019-08-08 15:11:29', 'User'),
	(3, 2, 'long@email.com', 'users/default.png', NULL, '$2y$10$e.IJf6.3noQQlfbpUATf2uojpVkcYOFkSLIt7u2.5xrlK/MdjIH0S', NULL, '{"locale":"en"}', '2019-08-08 15:11:17', '2019-08-08 15:11:17', 'Long');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table koreastore.user_infos
CREATE TABLE IF NOT EXISTS `user_infos` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_count` bigint(20) NOT NULL DEFAULT 0,
  `gender` binary(255) NOT NULL DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `dob` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.user_infos: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_infos` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_infos` ENABLE KEYS */;

-- Dumping structure for table koreastore.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koreastore.user_roles: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
REPLACE INTO `user_roles` (`user_id`, `role_id`) VALUES
	(3, 2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
