<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        Window.Laravel = {
            csrfToken: '{{ csrf_token() }}'
        }
    </script>
    <title></title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <div id="app">
        <main id="fe-body">
            <!-- Header -->
            <app-header></app-header>
            <!-- Header -->

            <!-- Body Content -->
            @yield('body-content')
            <!-- Body Content -->
                
            <!-- Footer -->
            <footer class="container-fluid bg-light py-md-5">
                <div class="container">
                    <div class="row aligns-item-center">
                        <address class="col-md-6 footer__column p-2 justify-content-center" style="font-size: 1.2rem">
                            <div>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span>
                                    20 Lê Lợi - Huế
                                </span>
                            </div>
                            <div>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span>
                                    0935.418.749
                                </span>
                            </div>
                            <div>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <span>
                                    nguyentd92@gmail.com
                                </span>
                            </div>
                        </address>
                        <div class="col-md-6 footer__column p-2">
                            <h5>About the company</h5>
                            <p>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odio
                                doloribus necessitatibus porro voluptatem consequatur deleniti
                                sint repudiandae reprehenderit voluptates a adipisci, maiores
                                repellendus, assumenda accusamus voluptate incidunt eius magnam
                                id!
                            </p>
                            <div style="font-size: 2rem">
                                <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
        </main>
    </div>
    <script src="{{ asset('js/app.js') }}">
    </script>
</body>

</html>