import Vue from "vue";
import Vuex from "vuex";
import axios from "axios"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        categories: [],
        cartItems: [],
        settings: {
            phone: '',
            address: '',
            email: '',
            brand: '',
            facebook: '',
            twitter: '',
            fanpage: '',
            info: ''
        },
        deals: []
    },
    getters: {
        countCategories: state => state.categories.length
    },
    mutations: {
        SET_CATEGORIES: (state, categories) => (state.categories = categories),
        SET_SETTINGS: (state, settings) => {
            settings.forEach(e => {
                state.settings[e.key.split('.')[1]] = e.value
            })
        },
        ADD_ITEM_TO_CART: (state, item) => {
            const result = state.cartItems.find(e => e.id == item.id)
            if (result) {
                result.quantity++
            } else {
                item.quantity = 1,
                    state.cartItems.push(item)
            }
            $cookies.set('cart', JSON.stringify(state.cartItems))
        },
        REMOVE_ITEM_FROM_CART: (state, item) => {
            state.cartItems = state.cartItems.filter(e => e.id != item.id)
            $cookies.set('cart', JSON.stringify(state.cartItems))
        },
        INCREASE_QUANTITY: (state, item) => {
            state.cartItems.find(e => e.id == item.id).quantity++ ,
                $cookies.set('cart', JSON.stringify(state.cartItems))
        },
        DESCREASE_QUANTITY: (state, item) => {
            const result = state.cartItems.find(e => e.id == item.id)
            if (result.quantity >1) {
                result.quantity--
                $cookies.set('cart', JSON.stringify(state.cartItems))
            }
        },
        LOAD_CART_LC: (state) => {

            state.cartItems = $cookies.isKey('cart') ? JSON.parse($cookies.get('cart')) : []
        },
        ACCEPT_ORDER: (state, user) => {
            axios.post('/api/ordering', {
                user,
                order: state.cartItems
            }).then(res => console.log(res))
        }
    },
    actions: {
        loadCartLC({commit}) {
            commit('LOAD_CART_LC')
        },
        addItem({ commit }, item) {
            commit('ADD_ITEM_TO_CART', item)
        },
        saveSettings({ commit }, settings) {
            commit('SET_SETTINGS', settings)
        },
        acceptOrder({commit}, user) {
            commit('ACCEPT_ORDER', user)
        }
    }
});
