/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Routes from './routes'
import VueCookies from 'vue-cookies'

import('./bootstrap');
import axios from 'axios'

import Vue from 'vue'
import 'mdbvue/build/css/mdb.css';
import 'mdbvue';
import App from '@/js/views/app'
import BootstrapVue from 'bootstrap-vue'
import store from "./store"

Vue.use(VueCookies)
Vue.use(BootstrapVue)

Vue.filter('toCurrency', function (value) {
    // if (typeof value !== "number") {
    //     return value;
    // }
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'VND',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
});

axios.interceptors.request.use(function (config) {
    // const token = this.$store.state.token;
    // if(token) {
    //     config.headers.Authorization = `Bearer ${token}`;
    // }
    config.headers.Accept = 'application/json'
    return config;
}, function (err) {
    return Promise.reject(err);
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// components
// Vue.component('home-carousel', require('./components/HomeCarousel.vue').default);
Vue.component('app-header', require('./components/AppHeader.vue').default);
Vue.component('item-card', require('./components/ItemCard.vue').default);
Vue.component('app-footer', require('./components/AppFooter.vue').default);
Vue.component('hp-category-group', require('./components/HPCategoryGroup.vue').default);
Vue.component('hp-category-list', require('./components/HPCategoryList.vue').default);
Vue.component('cart-item', require('./components/CartItem.vue').default);
Vue.component('cg-item', require('./components/CGItem.vue').default);

// pages
// Vue.component('items-page', require('./pages/itemspage.vue').default);
// Vue.component('home-page', require('./pages/HomePage.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    render: h => h(App),
    router: Routes,
    store,
    components: {
        // mdbBtn: mdbvue.mdbBtn
    }
    
});

export default app;
