import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/js/pages/HomePage'
import ItemsPage from '@/js/pages/ItemsPage'
import ItemDetails from '@/js/pages/ItemDetails'
import Cart from '@/js/pages/Cart'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: HomePage,
            name: 'home'
        },
        {
            path: '/items',
            component: ItemsPage,
            name: 'items',
        },
        {
            path: '/items/:id',
            component: ItemDetails,
            name: 'itemdetails'
        },
        {
            path: '/cart',
            component: Cart,
            name: 'cart'
        },
    ]
})

export default router;